// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCCSjl9V0m2Hzmyg3gWmHRD4wm-XxU7sh4",
    authDomain: "preparation-tamar.firebaseapp.com",
    projectId: "preparation-tamar",
    storageBucket: "preparation-tamar.appspot.com",
    messagingSenderId: "1010938538421",
    appId: "1:1010938538421:web:6d9d7a18dac5ab84b811fc"
  } 
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
