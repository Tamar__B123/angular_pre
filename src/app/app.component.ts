import { Component } from '@angular/core';

@Component({
  selector: 'app-root', //הסלקטור הוא בעצם השם של האלמנט של הויו
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent {
  title = 'preparation'; //הכותרת של העמוד
}
