import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Firstc } from './interfaces/firstc';
@Injectable({
  providedIn: 'root'
})
export class FirstcService {

  userCollection:AngularFirestoreCollection = this.db.collection('users');

  firstcs =[{name:'Tamar', age:'25', hobby:'dance'},
            {name:'David', age:'22', hobby:'travel'},
            {name:'Lena', age:'58', hobby:'read books'}] //מערך של ג'ייסון עם מפתחות וערכים

  public addFirstcs(userId:string, name:string, age:string){
    const firstc = {name:name, age:age};
    this.userCollection.doc(userId).collection('firstc').add(firstc);
  }

  firstcCollection:AngularFirestoreCollection;

  public getfirstcs(userId, startAfter){
    this.firstcCollection = this.db.collection(`users/${userId}/firstc`, 
    ref => ref.orderBy('name', 'asc').limit(4).startAfter(startAfter)); //בונה אפאנס לפירסטקולקשיין בעזרת גיבי קולקשיין והנתיב
    return this.firstcCollection.snapshotChanges() //מחזירים אובסרבבול
  }
  
  /*public getfirstcs(userId){
    this.firstcCollection = this.db.collection(`users/${userId}/firstc`); //בונה אפאנס לפירסטקולקשיין בעזרת גיבי קולקשיין והנתיב
    return this.firstcCollection.snapshotChanges().pipe(map( //הפונקציה סנפשוט בונה אובסרבבול שמעדכן אותנו על כל שינוי שיש ברשימת הפירסט
      collection =>collection.map( ///מקפ משתמשת לביצוע מניפולציות על הקולקשין
        document =>{
          const data = document.payload.doc.data(); //בונים משתנה שנקרא דאטה שיכיל את השדותת שם וגיל
          data.id = document.payload.doc.id; //אנחנו רוצים דם את האיידי כדי שנוכל לבצע פעולות על הפרטים לדוגמא מחיקה
          return data; //מחזיר משתנה בעל 3 שדות שהם איי די , שם וגיל.
        }
      )
    )) //מחזירים אובסרבבול
  }*/

  deleteFirstc(Userid:string ,id:string){ //במסד נתונים לא רלציוני אין קשר סין הפרטים של יוזר א לפרטים של יוזר ב, לכן צריך את המזהה של היוזר ושל הפריט בקולקשין
    this.db.doc(`users/${Userid}/firstc/${id}`).delete();
  }

  updateFirstc(userId:string, id:string, name:string, age:string){
    this.db.doc(`users/${userId}/firstc/${id}`).update(
     {
    name:name,
    age:age
     })
  }

  constructor(private db:AngularFirestore) { }
}
