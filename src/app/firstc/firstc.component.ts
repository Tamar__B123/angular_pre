import { Firstc } from './../interfaces/Firstc';
import { AuthService } from './../auth.service';
import { FirstcService } from './../firstc.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'firstc',
  templateUrl: './firstc.component.html',
  styleUrls: ['./firstc.component.css']
})
export class FirstcComponent implements OnInit {
  
  firstcs:Firstc[];
  editstate = [];
  userId:string;
  firstcs$; //יצירת משנה שהוא אובסרבבול - מוסכמה לשים דולר
  addFirstcFormOPen = false;
  lastDocumentArrived;  


  panelOpenState = false;

  add(firstc:Firstc){
    this.firstcService.addFirstcs(this.userId, firstc.name, firstc.age);
  }

  update(firstc:Firstc){
    this.firstcService.updateFirstc(this.userId, firstc.id, firstc.name, firstc.age)
  }

  deleteFirtc(id:string){
    this.firstcService.deleteFirstc(this.userId,id);
  }

  constructor(private firstcService:FirstcService, public authService:AuthService){} //depedency injection

  nextPage(){
    this.firstcs$ = this.firstcService.getfirstcs(this.userId, this.lastDocumentArrived); 
    this.firstcs$.subscribe(
      docs =>{
        this.lastDocumentArrived = docs[docs.length-1].payload.doc; 
        this.firstcs = [];
        for(let document of docs){
          const firstc:Firstc = document.payload.doc.data();
          firstc.id = document.payload.doc.id; 
          this.firstcs.push(firstc); 
        }
      }
    )     
  }

  ngOnInit(): void {//פונרקציה זו מופעלת אחרי שהאובייקט נוצר
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.firstcs$ = this.firstcService.getfirstcs(this.userId, null);
        this.firstcs$.subscribe(
          docs =>{
            this.lastDocumentArrived = docs[docs.length-1].payload.doc;
            this.firstcs = [];
            for (let document of docs){
              const firstc:Firstc = document.payload.doc.data();
              firstc.id = document.payload.doc.id;
              this.firstcs.push(firstc);
            }
          }
        )
      }
    )
  }

}
