import { Observable } from 'rxjs';
import { CoronaService } from './../corona.service';
import { Weather } from './../interfaces/weather';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-secondc',
  templateUrl: './secondc.component.html',
  styleUrls: ['./secondc.component.css']
})
export class SecondcComponent implements OnInit {
  
  city:string;
  temperature:number;
  image:string;
  country;
  lon:number; 
  lat:number;
  weatherData$:Observable<Weather>;
  hasError:Boolean = false;
  errorMessage:string;
  
  constructor(private route:ActivatedRoute, private coronaService:CoronaService) { }

  //הרשמה לאובסרססול לכן נשתמש בסבסקרייב
  ngOnInit(): void {
    this.city = this.route.snapshot.params.city;
    this.weatherData$ = this.coronaService.searchWeatherData(this.city);
    this.weatherData$.subscribe(
      data => {
        this.temperature = data.temperature;
        this.image = data.image;
        this.country = data.country;
        this.lon = data.lon;
        this.lat = data.lat; 
      },
      error =>{
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message;
      }
    )
  }

}
