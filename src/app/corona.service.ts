import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { Weather } from './interfaces/weather';
import { WeatherRaw } from './interfaces/weather-raw';

@Injectable({
  providedIn: 'root'
})
export class CoronaService {
  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "6f8d5b91bc8a33a8446f4b939ac9373e";
  private IMP = "units=metric";

  constructor(private http:HttpClient) { }

  searchWeatherData(cityName:string):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }


  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server error')
  }

  private transformWeatherData(data:WeatherRaw):Weather {
    return{
      name:data.name,
      country:data.sys.country,
      image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon
    }
  }
}