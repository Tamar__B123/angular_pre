import { Firstc } from './../interfaces/Firstc';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-first-form',
  templateUrl: './first-form.component.html',
  styleUrls: ['./first-form.component.css']
})
export class FirstFormComponent implements OnInit {

  @Input() name:string; //נתונים כשיכנסו דרך האבא
  @Input() age:string;
  @Input() id:string;
  @Input() formType:string;
  @Output() update = new EventEmitter<Firstc>(); 
  @Output() closeEdit = new EventEmitter<null>(); //האיוונט אימיטר מאפשר לנו להביא מאלמנט הבן פקודת סגירה לאלמנט האב

  updateParent(){
    let firstc:Firstc = {id:this.id, name:this.name, age:this.age};
    this.update.emit(firstc); //הפונקציה אמיט משדרת את הפירסט החוצה אל האב
    if(this.formType == "Add Firstc"){
      this.name = null;
      this.age = null;
    }
  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }

  constructor() { }

  ngOnInit(): void {
  }

}
