import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CityFormComponent } from './city-form/city-form.component';
import { ClassifyComponent } from './classify/classify.component';
import { FirstcComponent } from './firstc/firstc.component';
import { LoginComponent } from './login/login.component';
import { SecondcComponent } from './secondc/secondc.component';
import { SignUpComponent } from './sign-up/sign-up.component';

const routes: Routes = [
    { path: 'first', component: FirstcComponent },
    { path: 'second/:city', component: SecondcComponent }, 
    { path: 'classify/:network', component: ClassifyComponent },
    { path: 'city', component: CityFormComponent },
    { path: 'login', component: LoginComponent},
    { path: 'signup', component: SignUpComponent}  
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
